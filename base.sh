#!/bin/bash
# authors : https://gitlab.com/Mxaxax - https://gitlab.com/edricus
if [ "$EUID" -ne 0 ]
  then echo "Lancer en tant que root"
  exit
fi


###################
## USER CREATION ##
###################
printf "\033[32;5m== Creation des utilisateurs...\033[0m\n"

## BELLINUXIEN
if id "bellinuxien" >/dev/null 2>&1; then
  echo -e "\e[93ml'utilisateur bellinuxien existe déjà"
else
	if (whiptail --yesno "Voulez-vous créer l'utilisateur bellinuxien ?" 8 78  --title "Creation de l'utilisateur bellinuxien" 3>&1 1>&2 2>&3); then
  	adduser bellinuxien --gecos "" --disabled-password --quiet
    usermod -aG sudo bellinuxien
    newgrp sudo
  	PASSWORD=1
  	while [[ $PASSWORD != $VERIF ]]; do
  	  PASSWORD=$(whiptail --passwordbox "Entrez le mot de passe de bellinuxien" 8 78 --title "Creation de l'utilisateur bellinuxien" 3>&1 1>&2 2>&3)
  	  VERIF=$(whiptail --passwordbox "Entrez une nouvelle fois le mot de passe" 8 78 --title "Creation de l'utilisateur bellinuxien" 3>&1 1>&2 2>&3)
  	  if [[ $PASSWORD != $VERIF ]]; then
  	    whiptail --msgbox "Les mots de passe ne correspondent pas !" --title "Creation de l'utilisateur bellinuxien" 8 78
  	  fi
  	done
  	echo "bellinuxien:"$PASSWORD"" | chpasswd
  	usermod -aG sudo bellinuxien
  	cat << EOF > /var/lib/AccountsService/users/bellinuxien
[User]
SystemAccount=true
EOF
  fi
fi

## PERSO / VISITEUR
PERSO='1 1' # ugly workaround to start the space-in-string verfication loop
PASSWORD=1
if (whiptail --yesno "Voulez-vous créer un utilisateur ?" 8 78  --title "Creation d'un utilisateur perso" 3>&1 1>&2 2>&3); then
  while [[ ! -z $(echo $PERSO | grep "[[:space:]]") ]]; do
    PERSO="$(whiptail --inputbox --separate-output --title "Creation d'un utilistateur perso" 3>&1 1>&2 2>&3 \
      "Entrez le nom d'utilisateur à créer" 8 40 visiteur)"
    if [[ ! -z $(echo $PERSO | grep "[[:space:]]") ]]; then
      whiptail --msgbox "Le nom d'utilisateur ne doit pas contenir d'espaces !" --title "Creation d'un utilisateur perso" 8 78
    fi
  done
  if [[ $PERSO == visiteur ]]; then
    if id "visiteur" >/dev/null 2>&1; then
      echo -e "\e[93mL'utilisateur visiteur existe déjà"
    else
      adduser visiteur --gecos "visiteur" --disabled-password --quiet
      usermod -a -G cdrom,floppy,audio,bluetooth,dip,video,plugdev,scanner,netdev,lp,lpadmin visiteur
      echo 'visiteur:visiteur' | chpasswd
    fi
  else
    while [[ $PASSWORD != $VERIF ]]; do
      PASSWORD=$(whiptail --passwordbox "Entrez le mot de passe" 8 78 --title "Creation d'un utilisateur perso" 3>&1 1>&2 2>&3)
      VERIF=$(whiptail --passwordbox "Entrez une nouvelle fois le mot de passe" 8 78 --title "Creation d'un utilisateur perso" 3>&1 1>&2 2>&3)
      if [[ $PASSWORD != $VERIF ]]; then
        whiptail --msgbox "Les mots de passe ne correspondent pas !" --title "Creation d'un utilisateur perso" 8 78
      else
        adduser $PERSO --gecos "${PERSO^}" --disabled-password --quiet
        echo "$PERSO:"$PASSWORD"" | chpasswd
        if (whiptail --yesno "Voulez-vous ajouter cet utilisateur au groupe sudo ?" --title "Creation d'un utilisateur perso" 8 78 3>&1 1>&2 2>&3) then
          usermod -aG sudo $PERSO
        fi
      fi
    done
  fi
  else PERSO=$(id -nu 1000)
fi
if [[ -z $PERSO ]]; then
	PERSO=$(id -nu 1000)
fi


#On retire Firefox ESR & LibreOffice
sudo apt autoremove --purge firefox-esr* -y
sudo apt autoremove --purge libreoffice* -y

#####################
## SOURCES.LIST et update/upgrade##
#####################
printf "\033[0;32m== Configuration de APT... \033[0m\n"
cp files/sources.list /etc/apt/sources.list
# dpkg --add-architecture i386
apt-get update -y
apt-get upgrade -y

#############################
## INTERNET ACCESS CONTROL ##
#############################
echo $'[main]\ndns=none' | tee /etc/NetworkManager/conf.d/90-dns-none.conf &>/dev/null
systemctl reload NetworkManager
sed -i '1,4 s/^/#/' /etc/resolv.conf && sed -i -e '$anameserver 1.1.1.2\nnameserver 2606:4700:4700::1112' /etc/resolv.conf
systemctl reload NetworkManager
#cloudflare DNS Anti Malware


##################
## CONFIG FILES ##
##################
ACCOUNTSSERVICE="/var/lib/AccountsService/users/"$PERSO""

# BASHRC
printf "\033[0;32m== Modification de bashrc... \033[0m\n"
mv /home/"$PERSO"/.bashrc /home/"$PERSO"/.bashrc.BACK
cp -r files/bashrc /home/"$PERSO"/.bashrc

###################
## BASE PACKAGES ##
###################
printf "\033[0;32m== Installation des programmes de base... \033[0m\n"
apt-get install -yqq git curl wget zip unzip bash-completion flatpak  libxdo3 ttf-mscorefonts-installer
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo


##########################
## INSTALL DE ##

#Execute base/
# cd base/DE_Install.sh
# bash DE_Install.sh
# cd ..
###########


##########################
## USER PROFILES : TeamWorking, Gamer...##

# Execute base/user_profiles.sh
###########


##########################
# SOFTWARES INSTALLATION #
##########################

if ! dpkg -l | grep -q dbus-x11; then
    printf "\033[0;32m== dbus-x11 n'est pas installé. Installation en cours... \033[0m\n"
    sudo apt update -y
    sudo apt install -y dbus-x11
    printf "\033[0;32m== dbus-x11 installé avec succès.\033[0m\n"

fi

#Install softwares via Flatpak
printf "\033[0;32m== Installation des logiciels : Thunderbird, Element, Firefox, LibreOffice, VLC, Parabolic via Flatpak\033[0m\n"
#flatpak install -y flathub io.github.flattool.Warehouse org.mozilla.Thunderbird org.mozilla.firefox im.riot.Riot org.libreoffice.LibreOffice org.videolan.VLC #org.nickvision.tubeconverter
flatpak install -y flathub org.mozilla.firefox org.libreoffice.LibreOffice 
printf "\033[0;32m== Fin de l'installation ==\033[0m\n"


##########################
# Firefox #
# Execute base/firefox.sh
###########


echo "$USER, well done!"


#solution temporaire (ils en existent d'autres comme kill pour kill un processus mais dommageable pour le système je pense)
#sudo bash -c "sleep 10 && reboot"
 #Restart GNOME
 #su -c "busctl --user call "org.gnome.Shell" "/org/gnome/Shell" "org.gnome.Shell" "Eval" "s" 'Meta.restart("Restarting…")';" $PERSO
