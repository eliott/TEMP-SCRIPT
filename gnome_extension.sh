#!bin/bash

EXT_LIST=(
arcmenu@arcmenu.com
dash-to-panel@jderose9.github.com
)

for i in "${EXT_LIST[@]}"
do
    busctl --user call org.gnome.Shell.Extensions /org/gnome/Shell/Extensions org.gnome.Shell.Extensions InstallRemoteExtension s ${i}
done

dconf load /org/gnome/shell/extensions/ < files/gnome_extensions.conf
echo "Are you good?"
